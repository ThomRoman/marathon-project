
const copy = require('copy'),
  print = console.log

copy(
  './src/statics/**/*.*',
  './public/',
  err => err ? print(err.message) : print('Archivos copiados con éxito')
)