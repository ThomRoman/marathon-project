import Hammer from 'hammerjs'

const HAMBURGER = document.querySelector('.Panel-button'),
    PANEL = document.querySelector('.Panel'),
    MEDIA_QUERY = window.matchMedia('(min-width: 64em)'),
    hammerBody = new Hammer(document.body),
    hammerPanel = new Hammer(PANEL)


function closePanel(mq) {
    if (mq.matches) {
        PANEL.classList.remove('is-active')
        document.querySelector('.hamburger').classList.remove('is-active')
    }
}

function hammerTouches(e) {
    if (e.type == 'swipeleft') {
        PANEL.classList.remove('is-active')
        document.querySelector('.hamburger').classList.remove('is-active')
    } else if (e.type == 'swiperight') {
        PANEL.classList.add('is-active')
        document.querySelector('.hamburger').classList.add('is-active')
    }
}

MEDIA_QUERY.addListener(closePanel)
closePanel(MEDIA_QUERY)

HAMBURGER.addEventListener('click', event => {
    event.preventDefault()
    document.querySelector('.hamburger').classList.toggle('is-active')
    PANEL.classList.toggle('is-active')
})

hammerPanel.on('swipeleft  swiperight', hammerTouches)
hammerBody.on('swipeleft  swiperight', hammerTouches)