#

````sh
    npm run assets
    npm start # refresh sass
````

## **PUG**

- Se puede usar `include` para diferentes archivos, no solo para archivos jade. Además, puede usar incluir varias veces en la página. Este comando solo incluye contenido del
    archivo diferente, directamente en el lugar. Puede pensar en ello, como si hiciera un gran proyecto de Lego utilizando (incluidas) diferentes piezas pequeñas.
- `extends`, por el contrario, toma un gran proyecto de pug y sustituye los bloques marcados con tus piezas con un comando de bloque especial. Podría usarse una vez y 
    sobrescribir el contenido del archivo de destino (archivo con el comando extend)